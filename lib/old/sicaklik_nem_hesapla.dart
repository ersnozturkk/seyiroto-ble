import 'package:intl/intl.dart';

String sicaklikNemHesaplama(String tempRawStringData) {
  try {
    var c1 = tempRawStringData[2];
    var c2 = tempRawStringData[3];
    var c3 = tempRawStringData[0];
    var c4 = tempRawStringData[1];

    var swapHexStringData = c1 + c2 + c3 + c4;
    double tempFloatValue = 0;
    int rawdata = int.parse(swapHexStringData, radix: 16);
    var bitArray = rawdata.toRadixString(2);
    bitArray = bitArray.split('').reversed.join();
    String mask = "00000000000000000000000000000000";
    bitArray = bitArray + mask.substring(bitArray.length);

    tempFloatValue = double.parse(bitArray[0]) / 16 +
        double.parse(bitArray[1]) / 8 +
        double.parse(bitArray[2]) / 4 +
        double.parse(bitArray[3]) / 2;
    double tamsayikismi = double.parse(bitArray[4]) +
        2 * double.parse(bitArray[5]) +
        4 * double.parse(bitArray[6]) +
        8 * double.parse(bitArray[7]) +
        16 * double.parse(bitArray[8]) +
        32 * double.parse(bitArray[9]) +
        64 * double.parse(bitArray[10]);

    if (int.parse(bitArray[15]) == 1) {
      tempFloatValue = 1 - tempFloatValue;
      tempFloatValue = tempFloatValue + (127 - tamsayikismi);
    } else {
      tempFloatValue = tempFloatValue + tamsayikismi;
    }
    tempFloatValue = 100 * tempFloatValue;
    final f = new NumberFormat("#####");
    String stringTempData = f.format(tempFloatValue);
    if (stringTempData.length == 4) stringTempData = "0" + stringTempData;

    if (int.parse(bitArray[15]) == 1) {
      stringTempData = stringTempData.substring(1);
      stringTempData = "-" + stringTempData;
    }

    double sonuc = double.parse(stringTempData) / 100;
    return sonuc.toStringAsFixed(2);
  } catch (e) {
    return "-";
  }
}
