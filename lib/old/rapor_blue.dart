import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:reactiveble/controller/ble_controller.dart';

class RaporBlue extends StatefulWidget {
  final ScanResult scanResult;
  const RaporBlue({
    Key? key,
    required this.scanResult,
  }) : super(key: key);

  @override
  _RaporBlueState createState() => _RaporBlueState();
}

class _RaporBlueState extends State<RaporBlue> {
  final flutterBlue = FlutterBlue.instance;
  var deviceState = BluetoothDeviceState.disconnected.obs;
  final bleController = Get.put(BleController());
  @override
  void initState() {
    super.initState();
    widget.scanResult.device.state.listen((event) {
      print("Bağlanma Durumu : $event");
      deviceState.value = event;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          ListTile(
            title: Text(widget.scanResult.device.name +
                "- RSSI:" +
                widget.scanResult.rssi.toString()),
            subtitle: Text(widget.scanResult.advertisementData.manufacturerData
                .toString()),
          ),
          Obx(
            () => deviceState.value == BluetoothDeviceState.connected
                ? Icon(
                    Icons.bluetooth_connected,
                    color: Colors.blue,
                  )
                : deviceState.value == BluetoothDeviceState.connecting
                    ? Icon(
                        Icons.bluetooth_disabled,
                        color: Colors.green,
                      )
                    : Icon(
                        Icons.bluetooth_disabled,
                        color: Colors.red,
                      ),
          ),
          Obx(
            () => TextButton(
              onPressed: () {
                if (deviceState.value == BluetoothDeviceState.connected) {
                  baglanKopar();
                } else {
                  baglan();
                }
              },
              child: deviceState.value == BluetoothDeviceState.connected
                  ? Text("Bağlandı")
                  : deviceState.value == BluetoothDeviceState.connecting
                      ? Text("Bağlanıyor")
                      : Text("Bağlan"),
            ),
          ),
          showDateTime(),
          TextButton(
            onPressed: () async {
              await bleController.readRapor(widget.scanResult);
              await bleController.valueToModel();
              setState(() {});
            },
            child: Text("Rapor Oku"),
          ),
          TextButton(
            onPressed: () async {
              await bleController.valueToModel().then((value) {
                setState(() {});
              });
            },
            child: Text("Raporu"),
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: bleController.beaconData.length,
                itemBuilder: (context, index) {
                  return Text(bleController.beaconDataModel[index].toString());
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget showDateTime() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: TextButton(
                  onPressed: () {
                    DatePicker.showDateTimePicker(context,
                        showTitleActions: true,
                        onChanged: (date) {}, onConfirm: (date) {
                      bleController.notifierBeginDate.value = date;
                    },
                        currentTime: DateTime(2021, 6, 14, 12, 00, 00),
                        locale: LocaleType.tr);
                  },
                  child: Text(
                    'Başlangıç Tarihini seç',
                    style: TextStyle(color: Colors.blue),
                  )),
            ),
            Expanded(
                child: ValueListenableBuilder(
              valueListenable: bleController.notifierBeginDate,
              builder: (BuildContext context, DateTime? value, Widget? child) {
                return Text(bleController.notifierBeginDate.value.toString());
              },
            )),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: TextButton(
                  onPressed: () {
                    DatePicker.showDateTimePicker(context,
                        showTitleActions: true,
                        onChanged: (date) {}, onConfirm: (date) {
                      bleController.notifierEndDate.value = date;
                    },
                        currentTime: DateTime(2021, 6, 19, 12, 00, 00),
                        locale: LocaleType.tr);
                  },
                  child: Text(
                    'Bitiş Tarihini seç',
                    style: TextStyle(color: Colors.blue),
                  )),
            ),
            Expanded(
                child: ValueListenableBuilder(
              valueListenable: bleController.notifierEndDate,
              builder: (BuildContext context, DateTime? value, Widget? child) {
                return Text(bleController.notifierEndDate.value.toString());
              },
            )),
          ],
        ),
      ],
    );
  }

  baglan() {
    widget.scanResult.device.connect();
  }

  baglanKopar() {
    widget.scanResult.device.disconnect();
  }
}
