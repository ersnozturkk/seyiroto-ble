import 'dart:typed_data';
import 'package:convert/convert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:reactiveble/model/bleStatusModel.dart';
import 'package:reactiveble/old/sicaklik_nem_hesapla.dart';

class BleController extends GetxController {
  List<List<int>> beaconData = [];
  List<BeaconData> beaconDataModel = [];

  BluetoothCharacteristic? characteristicforwrite;
  BluetoothCharacteristic? characteristicforread;

  static DateTime? begindate;
  static DateTime? enddate;
  static String? txbuffer;
  static int controlRxbuffer = 0;
  var notifierBeginDate = ValueNotifier(begindate);
  var notifierEndDate = ValueNotifier(enddate);
  var notifiertxBuffer = ValueNotifier(txbuffer);
  var notifiercontrolRxBuffer = ValueNotifier(controlRxbuffer);
  String status = " ";
  int packetcount = 0;
  int framecount = 0;
  List<int> temp1ID = [];
  List<int> temp2ID = [];
  var temp1Value = 0.0;
  var temp2Value = 0.0;
  DateTime? date;
  bool doorStatus = false;
  List<int> bironcekipaketten = [];
  var isReading = false.obs;

  Future readRapor(ScanResult scanResult) async {
    /*SERVİS KARAKTRİSTİK OKUMA _ YAZMA İŞLEMLERİ*/
    beaconData.clear();
    List<int> requestInfoBetween2Date = [0x10, 0x03, 0x09, 0x01];
    /*TÜM VERİLER İÇİN ALTTAKİ 2 SATIR*/
    /* requestInfoBetween2Date.addAll([0, 0, 0, 0]);
    requestInfoBetween2Date.addAll([0xFF, 0xFF, 0xFF, 0xFF]); */
    /*TARİHE GÖRE GETİRİLECEK VERİLER İÇİN ALTTAKİ 2 SATIR*/
    requestInfoBetween2Date.addAll(datetoTimestamp(notifierBeginDate.value));
    requestInfoBetween2Date.addAll(datetoTimestamp(notifierEndDate.value));

    print(requestInfoBetween2Date);
    print(requestInfoBetween2Date.sublist(3));

    requestInfoBetween2Date
        .add(fChecksumCalculator(requestInfoBetween2Date.sublist(3), 9));
    print(requestInfoBetween2Date);
    notifiertxBuffer.value = requestInfoBetween2Date.toString();
    await scanResult.device.discoverServices().then((services) {
      //SERVİSE BAĞLANILIYOR VE DİNLENİYOR:
      print("Servis bilgileri Alındı.");
      services.forEach((service) async {
        if (service.uuid.toString().contains("7e400001")) {
          print("Servis Bulundu." + service.uuid.toString());
          for (BluetoothCharacteristic c in service.characteristics) {
            if (c.uuid.toString().contains("7e400003")) {
              characteristicforread = c;
              print(
                  "okuma karakteristiği : " + characteristicforread.toString());
            }
            if (c.uuid.toString().contains("7e400002")) {
              characteristicforwrite = c;
              print("yazma karakteristiği : " +
                  characteristicforwrite.toString());
            }
          }
        }
      });
    }).then((value) async {
      await characteristicforread!.setNotifyValue(true).then((value) async {
        print("Opened Notifies");
        await characteristicforwrite!.write(requestInfoBetween2Date);
        if (notifiercontrolRxBuffer.value == 0) {
          print("Dinlemeye Girdi;");
          characteristicforread?.value.listen(
            (value) {
              /*  beaconData.add(value); */
              if (listEquals(value, [32, 3, 2, 1, 1, 254]) == true) {
                status = "istek Başarılı";
              } else if (value.isNotEmpty && value[0] == 48 && value[1] == 0) {
                var islastframe = getFirstFrame(value);
                if (islastframe.length == 0)
                  print("Son paket");
                else {
                  bironcekipaketten = islastframe;
                  print("Son değil");
                }
                if (packetcount == 0) {
                  characteristicforwrite!
                      .write([0x10, 0x03, 0x01, 0x03, 0xFD]).then(
                          (value) => print("bir sonraki sector istendi."));
                }
              } /*else if (value.isNotEmpty && value[0] == 48 && value[1] != 0) {
                            bool islastframe = getOtherFrames(value);
                            if (islastframe == true)
                              print("Son paket");
                            else
                              print("Son değil");
                          }*/
              else if (value.isNotEmpty && value[0] == 48 && value[1] != 0) {
                List<int> nextframe = bironcekipaketten;
                nextframe.addAll(value.sublist(3));
                packetcount -= value[2];
                print(
                    "Sonraki pakette packetcount = " + packetcount.toString());

                var islastframe = getOtherFrames(nextframe);
                if (islastframe == [])
                  print("Son paket");
                else {
                  bironcekipaketten = islastframe;
                  print("Son değil");
                }
                if (packetcount == 0) {
                  characteristicforwrite!
                      .write([0x10, 0x03, 0x01, 0x03, 0xFD]).then(
                          (value) => print("bir sonraki sector istendi."));
                }
              } else {
                print("ValueLen: " + value.length.toString());
                print("Value-else: " + value.toString());
              }

              // notifiercontrolRxBuffer.value++;
            },
          );
        }
      });
    });
  }

  List<int> getFirstFrame(List<int> value) {
    print("Value-first: " + value.toString());
    packetcount = ByteData.view(Uint8List.fromList(value.sublist(2, 4)).buffer)
        .getInt16(0, Endian.little);
    framecount = value[4];
    packetcount -= framecount;
    temp1ID = value.sublist(5, 11);
    temp2ID = value.sublist(11, 17);
    print("packetcount" + framecount.toString());
    print("framecount" + framecount.toString());
    for (int i = 0;
        i < ((framecount - 15) - ((framecount - 15)) % 11) / 11;
        i++) {
      ///modunu çıkardım hata olmaması için
      List<int> logs = value.sublist((i * 11) + 20, (i * 11) + 20 + 11);

      try {
        temp1Value =
            double.parse(sicaklikNemHesaplama(hex.encode(logs.sublist(0, 2))));
      } catch (e) {
        temp1Value = -999;
      }

      try {
        temp2Value =
            double.parse(sicaklikNemHesaplama(hex.encode(logs.sublist(2, 4))));
      } catch (e) {
        temp2Value = -999;
      }
      date = timestamptoDate(logs.sublist(4, 8));
      doorStatus = logs[8] == 1 ? true : false;
      notifiercontrolRxBuffer.value++;
      //beaconData.add(BeaconData(temp1: temp1Value, temp2: temp2Value, date: date!, doorStatus: doorStatus));
      beaconData.add(logs);
    }
    int birsonrakiframegidecek =
        framecount - (((framecount - 15) - ((framecount - 15)) % 11) + 15);
    print("Kalan" +
        value.reversed
            .toList()
            .sublist(1, birsonrakiframegidecek + 1)
            .reversed
            .toList()
            .toString());
    if (packetcount == framecount) return [];
    return value.reversed
        .toList()
        .sublist(1, birsonrakiframegidecek + 1)
        .reversed
        .toList();
  }

  List<int> getOtherFrames(List<int> value) {
    print("Value-other: " + value.toString());

    for (int i = 0; i < (((value.length) - (value.length % 11)) / 11); i++) {
      ///modunu çıkardım hata olmaması için
      List<int> logs = value.sublist((i * 11), (i * 11) + 11);
      print("logs:" + logs.toString());
      try {
        temp1Value =
            double.parse(sicaklikNemHesaplama(hex.encode(logs.sublist(0, 2))));
      } catch (e) {
        temp1Value = -999;
      }

      try {
        temp2Value =
            double.parse(sicaklikNemHesaplama(hex.encode(logs.sublist(2, 4))));
      } catch (e) {
        temp2Value = -999;
      }

      //temp1Value = double.parse(sicaklikNemHesaplama(hex.encode(logs.sublist(0, 2))));
      //temp2Value = double.parse(sicaklikNemHesaplama(hex.encode(logs.sublist(2, 4))));
      date = timestamptoDate(logs.sublist(4, 8));
      doorStatus = logs[8] == 1 ? true : false;
      notifiercontrolRxBuffer.value++;
      //beaconData.add(BeaconData(temp1: temp1Value, temp2: temp2Value, date: date!, doorStatus: doorStatus));
      beaconData.add(logs);
    }
    int birsonrakiframegidecek =
        value.length - (((value.length) - ((value.length)) % 11));
    try {
      print("Kalan(Other)" +
          value.reversed
              .toList()
              .sublist(1, birsonrakiframegidecek)
              .reversed
              .toList()
              .toString());
    } catch (e) {
      print("Kalan [] aa");
    }

    if (packetcount == framecount) return [];
    try {
      return value.reversed
          .toList()
          .sublist(1, birsonrakiframegidecek)
          .reversed
          .toList();
    } catch (e) {
      return [];
    }
  }

  DateTime timestamptoDate(List<int> date) {
    var timeseconds = ByteData.view(Uint8List.fromList(date).buffer)
        .getInt32(0, Endian.little);
    //Timestamp to Date
    var timestamptodate = DateTime(1970, 1, 1);
    //print('Seconds : ' + timeseconds.toString());
    //print(timestamptodate.add(Duration(seconds: timeseconds - 10800)));
    return timestamptodate
        .add(Duration(hours: 2, seconds: timeseconds - 10800));
  }

  Iterable<int> datetoTimestamp(DateTime? date) {
    //print('Date: ' + date.toString());
    var beginTimestamp = (date!.millisecondsSinceEpoch ~/ 1000) + 10800;
    //print('Datetotimestamp(Decimal): ' + begin_timestamp.toString());
    var beginTimestampHex = beginTimestamp.toRadixString(16);
    //print('Datetotimestamp(Hex): ' + begin_timestamp_hex);
    var beginTimestampHexarray = hex.decode(beginTimestampHex);
    //print('TimeStamp Byte Array: ' + begin_timestamp_hexarray.toString());
    return beginTimestampHexarray.reversed;
  }

  int fChecksumCalculator(List<int> dataBeCalculated, int checksumSize) {
    int templateChecksum = 0;
    for (int counterChecksum = 0;
        counterChecksum < checksumSize;
        counterChecksum++) {
      templateChecksum = templateChecksum + dataBeCalculated[counterChecksum];
    }

    templateChecksum = 256 - (0x000000FF & templateChecksum);

    templateChecksum = (0x000000FF & templateChecksum);

    //print(TemplateChecksum);
    return templateChecksum;
  }

  Future valueToModel() async {
    //Burada beaconData ya gelen listeyi işleyecek ve modele atacak
    beaconDataModel = [];
    print("valueToModel Başladı + ${DateTime.now()}");
    for (var item in beaconData) {
      double temp1 = 0;
      double temp2 = 0;
      DateTime date;
      int door = 0;
      try {
        print("Sıcaklık Gönderilen: " + hex.encode(item.sublist(0, 2)));
        temp1 =
            double.parse(sicaklikNemHesaplama(hex.encode(item.sublist(0, 2))));
      } catch (e) {
        temp1 = -999;
      }
      try {
        print("Sıcaklık 2Gönderilen: " + hex.encode(item.sublist(2, 4)));
        temp2 =
            double.parse(sicaklikNemHesaplama(hex.encode(item.sublist(2, 4))));
      } catch (e) {
        temp2 = -999;
      }

      date = timestamptoDate(item.sublist(4, 8));

      try {
        door = int.parse(item[8].toString());
      } catch (e) {
        temp2 = -999;
      }

      beaconDataModel.add(
          BeaconData(temp1: temp1, temp2: temp2, date: date, doorStatus: door));
    }
    print("valueToModel Bitti + ${DateTime.now()}");
  }
}
