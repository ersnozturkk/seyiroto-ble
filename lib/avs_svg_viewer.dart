import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AVSSvgViewer extends StatefulWidget {
  final String? assetName;
  final String? networkUrl;
  final double height;
  final Color? color;
  final BoxFit fit;

  const AVSSvgViewer.asset({
    Key? key,
    required String path,
    this.height = 20,
    this.color,
    this.fit = BoxFit.contain,
  })  : assetName = path,
        networkUrl = null,
        super(key: key);
  const AVSSvgViewer.network({
    Key? key,
    required String url,
    this.height = 20,
    this.color,
    this.fit = BoxFit.contain,
  })  : assetName = null,
        networkUrl = url,
        super(key: key);

  @override
  _AVSSvgViewerState createState() => _AVSSvgViewerState();
}

class _AVSSvgViewerState extends State<AVSSvgViewer> {
  String? networkUrl;

  @override
  Widget build(BuildContext context) {
    return widget.assetName != null
        ? SvgPicture.asset(
            widget.assetName!,
            height: widget.height,
            color: widget.color,
            fit: widget.fit,
            placeholderBuilder: (context) {
              return const CircularProgressIndicator();
            },
          )
        : SvgPicture.network(
            widget.networkUrl!,
            height: widget.height,
            color: widget.color,
            fit: widget.fit,
            placeholderBuilder: (context) {
              return const CircularProgressIndicator();
            },
          );
  }
}
