import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:get/get.dart';
import 'package:reactiveble/setting_page.dart';

class HomePageBlue extends StatefulWidget {
  const HomePageBlue({Key? key}) : super(key: key);

  @override
  _HomePageBlueState createState() => _HomePageBlueState();
}

class _HomePageBlueState extends State<HomePageBlue> {
  final flutterBlue = FlutterBlue.instance;
  List<ScanResult> gelenResult = [];
  List<ScanResult> tempResultResult = [];
  ScanResult? tekCihaz;
  bool play = true;

  @override
  void initState() {
    super.initState();
    flutterBlue.isScanning.listen((event) {
      print("isScanning: $event");
      setState(() {});
      if (event == false) {
        flutterBlue.startScan(
          allowDuplicates: true,
        );
      }
    });
    flutterBlue.scanResults.listen((event) {
      //print("GelenCihaz: $event");
      tempResultResult = event;
    });
    gelenResultEkle();
  }

  gelenResultEkle() async {
    while (play) {
      gelenResult = [];
      for (var item in tempResultResult) {
        //D2:9C:03:57:B0:1F    --Yapılacak Cihaz
        //F8:CB:FC:1A:0B:7D    --Elimizdeki Syrbl7
        if (item.device.name.contains("SYRB")) {
          tekCihaz = item;
          gelenResult.add(item);
          setState(() {});
        }
      }
      await Future.delayed(Duration(seconds: 2));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Text("Veri Sayısı: ".toUpperCase() + gelenResult.length.toString()),
          Expanded(
            child: Container(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: gelenResult.length,
                itemBuilder: (BuildContext context, int index) {
                  ScanResult secili = gelenResult[index];
                  return Container(
                    margin: EdgeInsets.all(5),
                    child: ListTile(
                      onTap: () async {
                        await Get.to(() => SettingPage(scanResult: gelenResult[index]));
                        await flutterBlue.stopScan();
                      },
                      leading: CircleAvatar(
                      child: Text(secili.rssi.toString()),
                      ),
                      title: Text("${secili.device.name}"),
                      subtitle: Text(secili.device.id.toString()),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
