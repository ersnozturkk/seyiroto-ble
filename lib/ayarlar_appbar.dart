import 'package:flutter/material.dart';
import 'package:reactiveble/avs_svg_viewer.dart';

class AyarlarAppBar extends StatelessWidget {
  const AyarlarAppBar({Key? key}) : super(key: key);
  final Color defBlueColor=Colors.blue;
  final double defContainerMargin=20;

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Color(0xfff1f7ff),
      child: SafeArea(
          bottom: false,
          child: Column(
            children: [
              SizedBox(height: defContainerMargin),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    //padding: const EdgeInsets.all(10.0),
                    child: AVSSvgViewer.asset(
                      path: "assets/icons/logo-dark.svg",
                      height: 30,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "Ayarlar",
                  style: TextStyle(fontSize: 18, color: defBlueColor),
                ),
              ),
            ],
          )),
    );
  }
}
