import 'dart:io';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:reactiveble/setting_page.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static DatabaseHelper? _databaseHelper;
  static Database? _database;

  factory DatabaseHelper() {
    if (_databaseHelper == null) {
      return DatabaseHelper._internal();
    } else {
      return _databaseHelper!;
    }
  }

  DatabaseHelper._internal();

  Future<Database?> _getDatabase() async {
    if (_database == null) {
      _database = await _initializeDatabase();

      return _database;
    } else {
      return _database;
    }
  }

//bu kısım sqflite github sayfasından alındı.
  Future<Database> _initializeDatabase() async {
    var databasesPath = await getDatabasesPath();

    var path = join(databasesPath, "bleDB.db");
    var file = new File(path);
    print(path);

    var exists = await databaseExists(path);
    if (!exists) {
      print("Creating new copy from asset");
      await deleteDatabase(path);
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}

      // assetsten veritabanını kopyala

      ByteData data = await rootBundle.load(join('assets', 'bleDB.db'));

      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      await new File(path).writeAsBytes(bytes, flush: true);
    } else {
      await openDatabase(path, readOnly: false);
      print("Opening existing database");
    }
    // Databaseyi çalıştır
    var _db = await openDatabase(path, readOnly: false);
    print(path);
    return _db;
  }

  // Tablo İşlemleri

  // Burası bleList tablosu
  Future<List<DinamikListModel>> getDinamikList() async {
    List<DinamikListModel> values = [];
    var db = await _getDatabase();
    print("Gelen DB: $db");

    var sonuc = await db!.query("dinamikList").catchError((error) {
      print("Veritabanı Error:$error");
      /* _TypeError (type 'Null' is not a subtype of type 'FutureOr<List<Map<String, Object?>>>') */
    });
    //print("Gelen Map: $sonuc");

    for (var item in sonuc) {
      //print("item: $item");
      Map<String, dynamic> map = item;
      DinamikListModel model = DinamikListModel.fromMap(map);
      values.add(model);
      //print("model: $model");
    }

    //print("appdb:$values");
    //db.close();
    print("Veritabanı getDinamikList: ${values.length}");
    return values;
  }

  Future<bool> setDinamikValue(DinamikListModel model) async {
    var db = await _getDatabase();
    var sonuc = await db!.insert("dinamikList", model.toMap());
    print("Veritabanı setDinamikValue Ekleme Sonucu: $sonuc");
    return true;
  }

  Future deleteDinamikValue(int id) async {
    var db = await _getDatabase();

    var result = await db!.rawDelete('DELETE FROM dinamikList WHERE id = ?', ['$id']).catchError((onError) {
      print("GelenHata: $onError");
    });
    print("Veritabanı deleteDinamikValue Uzunlığu: $result");
    return result;
  }
}
