import 'dart:convert';
import 'dart:typed_data';

import 'package:convert/convert.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart' as barcode;
import 'package:flutter_blue/flutter_blue.dart';
import 'package:get/get.dart';

import 'package:reactiveble/avs-snackbar/avs_snackbar.dart';
import 'package:reactiveble/custom_button.dart';
import 'package:reactiveble/services/db.dart';
import 'package:reactiveble/settingsclasses.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

BluetoothService? service;
BluetoothCharacteristic? characteristicWrite;
BluetoothCharacteristic? characteristicRead;

// ignore: must_be_immutable
class SettingPage extends StatefulWidget {
  final ScanResult scanResult;
  SettingPage({
    Key? key,
    required this.scanResult,
  }) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  Color defBlueColor = Color(0xFF40b9f1);
  var deviceState = BluetoothDeviceState.disconnected.obs;
  final _formKey = GlobalKey<FormState>();
  RxList dinamikListModel = [
    /*  DinamikListModel(title: "” Master yayın", byteValue: "10041901010101083C003C0088130000010101010000000000000301D9", isClient: true),
    DinamikListModel(title: "Masster2", byteValue: "10041901010101083C000807B80B0000010101010000000000000301DE", isClient: true) */
  ].obs;
  @override
  void initState() {
    //TODO veri listesini çek sharedden
    super.initState();
    getDinamikList();
    baglan();
    widget.scanResult.device.state.listen((event) {
      print("Bağlanma Durumu : $event");
      deviceState.value = event;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool nofifylistening = false; //Ayarları ekrana basmak için bu yapıyı değiştirebilirsin.
    var showsettings = ValueNotifier(nofifylistening);
    final Color buttonColor = Colors.blue;
    return WillPopScope(
      onWillPop: () async {
        Future.microtask(() {
          baglanKopar();
        });
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Cihaz Ayarları"),
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () async {
                  print("yenile");
                  showsettings.value = false;
                  await characteristicWrite?.write([0x10, 0x02, 0x01, 0x01, 0xFF]);
                },
                icon: Icon(Icons.refresh)),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                Obx(() {
                  if (deviceState.value == BluetoothDeviceState.connected) {
                    return TextButton(onPressed: () => baglanKopar(), child: Text("Bağlantıyı Kopar"));
                  } else {
                    return TextButton(onPressed: () => baglan(), child: Text("Bağlan"));
                  }
                }),
                Obx(() => valueCard("Bağlantı Durumu: ", deviceState.value == BluetoothDeviceState.connected ? "Cihaz Bağlı" : "Cihaz Bağlı Değil")),
                valueCard("Cihaz Adı: ", widget.scanResult.device.name),
                valueCard("Cihaz ID: ", widget.scanResult.device.id.toString()),
                Divider(color: Color(0XFFd1d1d1)),
                Obx(() {
                  if (deviceState.value == BluetoothDeviceState.connected) {
                    return FutureBuilder(
                        future: initSettings(),
                        builder: (BuildContext context, AsyncSnapshot<String?> futurevalue) {
                          if (futurevalue.hasData) {
                            return FutureBuilder(
                              future: characteristicRead?.setNotifyValue(true),
                              builder: (BuildContext context, AsyncSnapshot<bool?> futurenotifyvalue) {
                                if (futurenotifyvalue.hasData && futurenotifyvalue.data == true) {
                                  if (nofifylistening == false) {
                                    nofifylistening = true;
                                    settings = new Settings();
                                    characteristicRead?.value.listen((value) async {
                                      if (showsettings.value == false) {
                                        if (value.toString().contains("32, 2")) {
                                          settings.currenttime = timestamptoDate(value.sublist(3, 7));
                                          print("currenttime:" + settings.currenttime.toString());
                                          await characteristicWrite?.write([0x10, 0x05, 0x01, 0x01, 0xFF]);
                                        } else if (value.toString().contains("32, 5")) {
                                          getchangeablesettings(value);
                                          await characteristicWrite?.write([0x10, 0x04, 0x01, 0x02, 0xFE]);
                                        } else if (value.toString().contains("32, 4")) {
                                          getadvertisingsettings(value);
                                          await characteristicWrite?.write([0x10, 0x08, 0x01, 0x01, 0xFF]);
                                        } else if (value.toString().contains("32, 8")) {
                                          settings.firstrunningtime = timestamptoDate(getfirstrunningtime(value));
                                          print("firstrunningtime:" + settings.firstrunningtime.toString());
                                          await characteristicWrite?.write([0x10, 0x07, 0x01, 0xFF, 0x01]);
                                        } else if (value.toString().contains("32, 7")) {
                                          settings.battaryvoltage = getbattaryvoltage(value);
                                          print("battaryvoltage:" + settings.battaryvoltage.toString());
                                          await characteristicWrite?.write([0x10, 0x0A, 0x01, 0xFF, 0x01]);
                                        } else if (value.toString().contains("32, 10")) {
                                          settings.applicationVersion = getApplicationversion(value);
                                          print("applicationVersion:" + settings.applicationVersion.toString());
                                          settings.softDeviceversion = getSoftdeviceversion(value);
                                          print("softDeviceversion:" + settings.softDeviceversion.toString());

                                          /*TÜM AYARLAR ALINDI:*/
                                          showsettings.value = true;
                                        } else {
                                          //başlangıçta da yaz burdan sil
                                          await characteristicWrite?.write([0x10, 0x02, 0x01, 0x01, 0xFF]);
                                          print("Başlangıç");
                                        }
                                      } else {
                                        print("Vaalue: " + value.toString());
                                        if (value.toString().contains("[32, 4, 2, 1, 1, 254]")) {
                                          String deger = "Ayar Başarıyla Yüklendi";
                                          print(deger);

                                          print("yenile");
                                          showsettings.value = false;
                                          await characteristicWrite?.write([0x10, 0x02, 0x01, 0x01, 0xFF]);
                                          Future.microtask(() => showAVSSnackBar(context, AVSSnackBarChild.success(message: deger)));
                                        } else if (value.toString().contains("[32, 1, 1, 1, 255]")) {
                                          String deger = "Zaman Güncellendi";
                                          print(deger);
                                          print("yenile");
                                          showsettings.value = false;
                                          await characteristicWrite?.write([0x10, 0x02, 0x01, 0x01, 0xFF]);
                                          Future.microtask(() => showAVSSnackBar(context, AVSSnackBarChild.success(message: deger)));
                                        } else if (value.toString().trim().contains("[32, 5, 2, 2, 1, 253]")) {
                                          String deger = "Parametre Temizleme Başarılı";
                                          print(deger);
                                          Future.microtask(() => showAVSSnackBar(context, AVSSnackBarChild.success(message: deger)));
                                        } else if (value.toString().contains("[32, 5, 2, 2, 2, 253]")) {
                                          String deger = "Parametre Temizleme Başarısız";
                                          print(deger);
                                          Future.microtask(() => showAVSSnackBar(context, AVSSnackBarChild.success(message: deger)));
                                        } else if (value.toString().contains("[32, 18, 1, 1, 255]")) {
                                          String deger = "Kayıt Temizleme Başarılı";
                                          print(deger);
                                          Future.microtask(() => showAVSSnackBar(context, AVSSnackBarChild.success(message: deger)));
                                        } else if (value.toString().contains("[32, 18, 1, 2, 255]")) {
                                          String deger = "Kayıt Temizleme Başarısız";
                                          print(deger);
                                          Future.microtask(() => showAVSSnackBar(context, AVSSnackBarChild.success(message: deger)));
                                        }
                                      }
                                    });
                                  }
                                }

                                return ValueListenableBuilder(
                                    valueListenable: showsettings,
                                    builder: (BuildContext context, value, widget) {
                                      if (value == true) {
                                        return SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  IconButton(
                                                    icon: Icon(Icons.update_sharp),
                                                    onPressed: () async {
                                                      //1001042E7FC8602B
                                                      Future.microtask(() async {
                                                        List<int> timebuffer = [0x10, 0x01, 0x04];
                                                        timebuffer.addAll(datetoTimestamp(DateTime.now()));
                                                        timebuffer.add(checksumCalculator(timebuffer.sublist(3), timebuffer.length - 3));
                                                        print(timebuffer);
                                                        await characteristicWrite?.write(timebuffer).then((value) async {
                                                          showsettings.value = false;
                                                          await characteristicWrite?.write([0x10, 0x02, 0x01, 0x01, 0xFF]);
                                                        });
                                                      });
                                                    },
                                                  ),
                                                  Text("Dev. Time:"),
                                                  Text(settings.currenttime.toString()),
                                                  Text("   Fark:" + settings.currenttime!.difference(DateTime.now()).inMinutes.toString() + "dk"),
                                                ],
                                              ),
                                              degerCard("External Watchdog Reset", settings.externalwawtchdogresetcount.toString()),
                                              degerCard("İnternal Watchdog Reset", settings.internalwawtchdogresetcount.toString()),
                                              degerCard("Soft Reset", settings.softresetcount.toString()),
                                              degerCard("CPU Lock Up Reset", settings.cPULockUPresetcount.toString()),
                                              degerCard("Connectioned Time With AP", settings.connectionedtimewithAP!.inSeconds.toString() + " sec."),
                                              degerCard("Downloaded File Count", settings.downloadedfilecount.toString()),
                                              degerCard("Total Downloading File Time", settings.totaldownloadingfiletime!.inSeconds.toString() + " sec."),
                                              degerCard("Total Advertising Count", settings.totaladvertisingcount.toString()),
                                              degerCard(
                                                  "Advertising Period", settings.advertisingPeriod!.inMilliseconds > 1000 ? (settings.advertisingPeriod!.inSeconds.toString() + "sec.") : (settings.advertisingPeriod!.inMilliseconds.toString() + "msec.")),
                                              degerCard("Device Type", getDeviceTypeString(settings.deviceType ?? 0)),
                                              degerCard("Device Connection Type", settings.deviceConnectiontype.toString()),
                                              degerCard("Log Period", settings.logPeriod!.inMinutes.toString() + "min."),
                                              degerCard("Tx Power", settings.txPower.toString()),
                                              degerCard("First Running Time", settings.firstrunningtime.toString()),
                                              degerCard("Battery Voltage", settings.battaryvoltage!.toStringAsFixed(2).toString()),
                                              degerCard("Application Version", settings.applicationVersion!.toStringAsFixed(2).toString()),
                                              degerCard("Soft Device Version", settings.softDeviceversion.toString()),

                                              // Text("externalwawtchdogresetcount: " + settings.externalwawtchdogresetcount.toString()),
                                              // Text("internalwawtchdogresetcount: " + settings.internalwawtchdogresetcount.toString()),
                                              // Text("softresetcount: " + settings.softresetcount.toString()),
                                              // Text("CPULockUpResetCount: " + settings.cPULockUPresetcount.toString()),
                                              // Text("connectionedtimewithAP: " + settings.connectionedtimewithAP!.inSeconds.toString() + " sec."),
                                              // Text("downloadedfilecount: " + settings.downloadedfilecount.toString()),
                                              // Text("totaldownloadingfiletime: " + settings.totaldownloadingfiletime!.inSeconds.toString() + " sec."),
                                              // Text("totaladvertisingcount: " + settings.totaladvertisingcount.toString()),
                                              // Text("advertisingPeriod: " + settings.advertisingPeriod!.inSeconds.toString() + "sec."),
                                              // Text("deviceType: " + getDeviceTypeString(settings.deviceType ?? 0)),
                                              // Text("deviceConnectiontype: " + settings.deviceConnectiontype.toString()),
                                              // Text("logPeriod: " + settings.logPeriod!.inMinutes.toString() + "min."),
                                              // Text("txPower: " + settings.txPower.toString()),
                                              // Text("firstrunningtime: " + settings.firstrunningtime.toString()),
                                              // Text("battaryvoltage: " + settings.battaryvoltage!.toStringAsFixed(2).toString()),
                                              // Text("applicationVersion: " + settings.applicationVersion!.toStringAsFixed(2).toString()),
                                              // Text("softDeviceversion: " + settings.softDeviceversion.toString()),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite?.write([0x10, 0x0C, 0x01, 0xFF, 0x01]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("Kayıt Temizle"),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite?.write([0x10, 0x05, 0x01, 0x02, 0xFE]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("Param. Temizle"),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite
                                                            ?.write([0x10, 0x04, 0x19, 0x01, 0x02, 0x02, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x7F]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("T"),
                                                      //1.	Dorse Eşleşme “Trailer”
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite
                                                            ?.write([0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01, 0x80]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("H"),
                                                      //2.	Harici Sıcaklık Sensörlü “Harici”
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite
                                                            ?.write([0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x01, 0x7F]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("D"),
                                                      //3.	Dahili Sıcaklık Sensörlü “Dahili”
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite
                                                            ?.write([0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xF4, 0x01, 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01, 0x60]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("MH"),
                                                      //4.	Master Harici Sıcaklık Sensörlü “Master harici”
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        Future.microtask(() async => await characteristicWrite
                                                            ?.write([0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xF4, 0x01, 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x01, 0x5F]));
                                                      },
                                                      color: buttonColor,
                                                      child: Text("MD"),
                                                      //5.	Master Dahili Sıcaklık Sensörlü “Master dahili”
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        //10 04 19 01 01 01 01 08 3C 00 58 02 D0 07 00 00 02 01 01 01 00 00 00 00 00 00 02 01 83
                                                        Future.microtask(() {
                                                          List<int> buff = [0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x02, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01];
                                                          // List<int> macaddressQR = [0xd4, 0xfd, 0x72, 0x1f, 0x3d, 0xfa];
                                                          readQR().then((macaddressQR) async {
                                                            if (macaddressQR.length != 0) {
                                                              await characteristicWrite?.write(getbufferwithmacaddress(buff, macaddressQR));
                                                            }
                                                          });
                                                        });
                                                      },
                                                      color: buttonColor,
                                                      child: Text("CHI"),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        //10041901010101083C005802D0070000020101010000000000000301
                                                        Future.microtask(() {
                                                          List<int> buff = [0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x02, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x01];
                                                          // List<int> macaddressQR = [0xd4, 0xfd, 0x72, 0x1f, 0x3d, 0xfa];
                                                          // List<int> macaddressQR = await readQR();
                                                          readQR().then((macaddressQR) async {
                                                            if (macaddressQR.length != 0) {
                                                              await characteristicWrite?.write(getbufferwithmacaddress(buff, macaddressQR));
                                                            }
                                                          });
                                                        });
                                                      },
                                                      color: buttonColor,
                                                      child: Text("CDI"),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        //10041901010101083C005802D0070000020101010000000000000202
                                                        Future.microtask(() {
                                                          List<int> buff = [0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x02, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x02];
                                                          // List<int> macaddressQR = [0xd4, 0xfd, 0x72, 0x1f, 0x3d, 0xfa];
                                                          readQR().then((macaddressQR) async {
                                                            if (macaddressQR.length != 0) {
                                                              await characteristicWrite?.write(getbufferwithmacaddress(buff, macaddressQR));
                                                            }
                                                          });
                                                        });
                                                      },
                                                      color: buttonColor,
                                                      child: Text("CHH"),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 3,
                                                  ),
                                                  Expanded(
                                                    child: MaterialButton(
                                                      onPressed: () async {
                                                        //10041901010101083C005802D007000002010101000000000000030281

                                                        List<int> buff = [0x10, 0x04, 0x19, 0x01, 0x01, 0x01, 0x01, 0x08, 0x3C, 0x00, 0x58, 0x02, 0xD0, 0x07, 0x00, 0x00, 0x02, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x02];
                                                        // List<int> macaddressQR = [0xd4, 0xfd, 0x72, 0x1f, 0x3d, 0xfa];
                                                        readQR().then((macaddressQR) async {
                                                          if (macaddressQR.length != 0) {
                                                            await characteristicWrite?.write(getbufferwithmacaddress(buff, macaddressQR));
                                                          }
                                                        });
                                                      },
                                                      color: buttonColor,
                                                      child: Text("CDH"),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        );
                                      }
                                      return Container(margin: EdgeInsets.all(30), child: CircularProgressIndicator());
                                    });
                              },
                            );
                          }
                          return Text("Bekleniyor...");
                        });
                  } else {
                    //showsettings.value = false; //olmadı.
                    return Text("Baglanti yok...");
                  }
                }),
                Container(
                    margin: EdgeInsets.all(4),
                    child: Divider(
                      color: Color(0xffd1d1d1),
                    )),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        "Dinamik Sorgu Listesi",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    // IconButton(onPressed: () async {}, icon: Icon(Icons.refresh)),
                    IconButton(
                        onPressed: () async {
                          dinamikVeriEkle();
                        },
                        icon: Icon(Icons.add)),
                  ],
                ),
                Obx(() {
                  if (dinamikListModel.isEmpty) {
                    return Text("Dinamik Sorgu Bulunamadı.");
                  } else {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: dinamikListModel.length,
                      itemBuilder: (BuildContext context, int index) {
                        DinamikListModel seciliModel = dinamikListModel[index];
                        return Container(
                          margin: EdgeInsets.only(top: index == 0 ? 0 : 5, bottom: 5),
                          child: Row(
                            children: [
                              Expanded(
                                child: CustomButton(
                                  buttonRadius: 10,
                                  buttonColor: defBlueColor,
                                  buttonText: seciliModel.title,
                                  onPress: () async {
                                    if (deviceState.value == BluetoothDeviceState.connected) {
                                      List<int> byteList = [];
                                      Future.microtask(() {
                                        for (var i = 0; i < seciliModel.byteValue.length - 1; i = i + 2) {
                                          byteList.add(int.parse(seciliModel.byteValue.substring(i, i + 2), radix: 16));
                                        }

                                        if (seciliModel.isClient) {
                                          //QR okumalı
                                          readQR().then((macaddressQR) async {
                                            if (macaddressQR.length != 0) {
                                              await characteristicWrite?.write(getbufferwithmacaddressDinamik(byteList, macaddressQR));
                                            }
                                          });
                                        } else {
                                          //qr Okumasız
                                          // byteList.add(checksumCalculator(byteList.sublist(3), byteList.sublist(3).length));
                                          Future.microtask(() async => await characteristicWrite?.write(byteList));
                                        }
                                      });
                                    } else {
                                      print("Cihaz Bağlı Değil");
                                      showAVSSnackBar(context, AVSSnackBarChild.info(message: "Lütfen önce cihaza bağlanın."));
                                    }
                                  },
                                  height: 40,
                                ),
                              ),
                              IconButton(
                                onPressed: () async {
                                  DatabaseHelper _helper = DatabaseHelper();
                                  bool sil = false;
                                  await Alert(
                                      closeFunction: null,
                                      buttons: [
                                        DialogButton(
                                            color: Colors.red,
                                            child: Text(
                                              "Sil",
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            onPressed: () {
                                              sil = true;
                                              Navigator.pop(context);
                                            }),
                                        DialogButton(
                                            color: defBlueColor,
                                            child: Text(
                                              "İptal",
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            onPressed: () {
                                              sil = false;
                                              Navigator.pop(context);
                                            }),
                                      ],
                                      context: context,
                                      style: AlertStyle(titleStyle: TextStyle(color: Colors.black)),
                                      type: AlertType.info,
                                      content: Text(
                                        "${seciliModel.title}\nSilmek istiyor musunuz?",
                                        style: TextStyle(height: 1.5, fontSize: 14),
                                      )).show();
                                  if (sil) {
                                    await deleteDinamikValue(seciliModel.id!);

                                    await Future.delayed(Duration(seconds: 1));
                                  }
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  }
                })
              ],
            ),
          ),
        ),
      ),
    );
  }

  getDinamikList() async {
    dinamikListModel.value = [];
    DatabaseHelper _helper = DatabaseHelper();
    dinamikListModel.value = await _helper.getDinamikList();
    print("Dinamik List Geldi. Uzunluğu: ${dinamikListModel.length}");
  }

  setDinamikValue(DinamikListModel model) async {
    DatabaseHelper _helper = DatabaseHelper();
    await _helper.setDinamikValue(model).then((value) {
      return getDinamikList();
    });
  }

  deleteDinamikValue(int id) async {
    DatabaseHelper _helper = DatabaseHelper();
    await _helper.deleteDinamikValue(id).then((value) => getDinamikList());
    print("Alert Dialog Gelecek Rapor Silindi Diye result:");
  }

  Padding degerCard(String title, String value) {
    final double pad = 4;
    return Padding(
      padding: EdgeInsets.only(top: pad, bottom: pad),
      child: Row(
        children: [
          Expanded(flex: 4, child: Text("$title: ")),
          Expanded(flex: 3, child: Text(value)),
        ],
      ),
    );
  }

  Future<String?> initSettings() async {
    Future<String>? result;
    result = widget.scanResult.device.discoverServices().then((value) async {
      service = value.firstWhere((element) => element.uuid.toString().contains("7e400001") == true);
      characteristicRead = service!.characteristics.firstWhere((element) => element.uuid.toString().contains("7e400003") == true);
      characteristicWrite = service!.characteristics.firstWhere((element) => element.uuid.toString().contains("7e400002") == true);
    }).then((value) async {
      return "OK";
    });

    return result;
  }

  Widget valueCard(String title, String result) {
    return Row(
      children: [Expanded(flex: 2, child: Text(title)), Expanded(flex: 3, child: Text(result))],
    );
  }

  baglan() {
    widget.scanResult.device.connect();
  }

  baglanKopar() {
    widget.scanResult.device.disconnect();
  }

  dinamikVeriEkle() {
    String title = "", byteCode = "";
    var isClient = false.obs;

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        // onChanged: (value) => title = value,
                        decoration: InputDecoration(hintText: "Sorgu Adı"),
                        validator: (value) => GetUtils.isLengthGreaterThan(value, 3) ? null : "En az 4 Karakter",
                        onSaved: (value) => title = value!,
                      ),
                      TextFormField(
                        // onChanged: (value) => byteCode = value,
                        decoration: InputDecoration(hintText: "Sorgu Kodu"),
                        validator: (value) => GetUtils.isLengthGreaterThan(value, 3) ? null : "En az 4 Karakter",
                        onSaved: (value) => byteCode = value!,
                      ),
                      Row(
                        children: [
                          Text("Client: "),
                          Obx(() => Checkbox(
                              value: isClient.value,
                              onChanged: (e) {
                                isClient.value = e!;
                              })),
                        ],
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: CustomButton(
                        buttonRadius: 10,
                        buttonText: "Kaydet",
                        onPress: () async {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState!.save();
                            DinamikListModel model = DinamikListModel(title: title, byteValue: byteCode, isClient: isClient.value);
                            // dinamikListModel.add(model);
                            setDinamikValue(model);
                            print(model.toString());
                            Get.back();
                          }
                        },
                        height: 40,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: CustomButton(
                        buttonRadius: 10,
                        buttonText: "İptal",
                        onPress: () async {
                          Get.back();
                        },
                        height: 40,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

Iterable<int> datetoTimestamp(DateTime? date) {
  //print('Date: ' + date.toString());
  var beginTimestamp = (date!.millisecondsSinceEpoch ~/ 1000) + 10800;
  //print('Datetotimestamp(Decimal): ' + begin_timestamp.toString());
  var beginTimestampHex = beginTimestamp.toRadixString(16);
  //print('Datetotimestamp(Hex): ' + begin_timestamp_hex);
  var beginTimestampHexarray = hex.decode(beginTimestampHex);
  //print('TimeStamp Byte Array: ' + begin_timestamp_hexarray.toString());
  return beginTimestampHexarray.reversed;
}

DateTime timestamptoDate(List<int> date) {
  var timeseconds = ByteData.view(Uint8List.fromList(date).buffer).getInt32(0, Endian.little);
  //Timestamp to Date
  var timestamptodate = DateTime(1970, 1, 1);
  //print('Seconds : ' + timeseconds.toString());
  //print(timestamptodate.add(Duration(seconds: timeseconds - 10800)));
  return timestamptodate.add(Duration(hours: 2, seconds: timeseconds - 10800));
}

int checksumCalculator(List<int> dataBeCalculated, int checksumSize) {
  int templateChecksum = 0;
  for (int counterChecksum = 0; counterChecksum < checksumSize; counterChecksum++) {
    templateChecksum = templateChecksum + dataBeCalculated[counterChecksum];
  }

  templateChecksum = 256 - (0x000000FF & templateChecksum);

  templateChecksum = (0x000000FF & templateChecksum);

  //print(TemplateChecksum);
  return templateChecksum;
}

void getchangeablesettings(List<int> value) {
  settings.externalwawtchdogresetcount = getexternalwatchdogresetcount(value);
  //print("externalwawtchdogresetcount:" + settings.externalwawtchdogresetcount.toString());
  settings.internalwawtchdogresetcount = getinternalwatchdogresetcount(value);
  //print("internalwawtchdogresetcount:" + settings.internalwawtchdogresetcount.toString());
  settings.softresetcount = getsoftresetcount(value);
  //print("softresetcount:" + settings.softresetcount.toString());
  settings.cPULockUPresetcount = getCpulockupresetcount(value);
  //print("CPULockUPresetcount:" + settings.cPULockUPresetcount.toString());
  settings.connectionedtimewithAP = Duration(seconds: getconnectionedtimewithAP(value));
  //print("connectionedtimewithAP:" + settings.connectionedtimewithAP.toString());
  settings.downloadedfilecount = getdownloadedfilecount(value);
  //print("downloadedfilecount:" + settings.downloadedfilecount.toString());
  settings.totaldownloadingfiletime = Duration(seconds: gettotaldownloadingfiletime(value));
  //print("totaldownloadingfiletime:" + settings.totaldownloadingfiletime.toString());
  settings.totaladvertisingcount = gettotaladvertisingtime(value);
}

void getadvertisingsettings(List<int> value) {
  int ismaster = value[17];
  settings.advertisingPeriod = Duration(milliseconds: getadvertisingperiod(value));
  print("advertisingPeriod:" + settings.advertisingPeriod.toString());
  settings.deviceType = getdevicetype(value);
  print("deviceType:" + settings.deviceType.toString());
  settings.txPower = getTxPower(value);
  print("txPower:" + settings.txPower.toString());
  settings.logPeriod = Duration(seconds: getlogperiod(value)); //dk olarak gösterilecek
  print("logPeriod:" + settings.logPeriod.toString());

  if (settings.advertisingPeriod!.inMilliseconds == 500 && ismaster == 1) {
    settings.deviceConnectiontype = "Master";
  } else if (ismaster == 2) {
    settings.deviceConnectiontype = "Client";
  } else if (settings.advertisingPeriod!.inMilliseconds != 500) {
    settings.deviceConnectiontype = "Standart";
  }
}

int getexternalwatchdogresetcount(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(4, 8)).buffer).getInt32(0, Endian.little);
}

int getinternalwatchdogresetcount(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(8, 12)).buffer).getInt32(0, Endian.little);
}

int getsoftresetcount(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(12, 16)).buffer).getInt32(0, Endian.little);
}

int getCpulockupresetcount(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(16, 20)).buffer).getInt32(0, Endian.little);
}

int getconnectionedtimewithAP(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(20, 24)).buffer).getInt32(0, Endian.little);
}

int getdownloadedfilecount(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(24, 28)).buffer).getInt32(0, Endian.little);
}

int gettotaldownloadingfiletime(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(28, 32)).buffer).getInt32(0, Endian.little);
}

int gettotaladvertisingtime(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(32, 36)).buffer).getInt32(0, Endian.little);
}

//2.ayarlar

int getadvertisingperiod(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(13, 17)).buffer).getInt32(0, Endian.little);
}

int getdevicetype(List<int> buffer) {
  return buffer[27];
}

int getlogperiod(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(11, 13)).buffer).getInt16(0, Endian.little);
}

List<int> getfirstrunningtime(List<int> buffer) {
  return buffer.sublist(3, 7);
}

double getbattaryvoltage(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(3, 7)).buffer).getFloat32(0, Endian.little);
}

double getApplicationversion(List<int> buffer) {
  return ByteData.view(Uint8List.fromList(buffer.sublist(3, 7)).buffer).getFloat32(0, Endian.little);
}

String getSoftdeviceversion(List<int> buffer) {
  return hex.encode(buffer.sublist(7, 9));
}

int getTxPower(List<int> buffer) {
  return buffer[8];
}

List<int> getbufferwithmacaddress(List<int> buffer, List<int> macAddress) {
  print("Buffer  qr'lı: " + buffer.toString());

  buffer.replaceRange(20, 26, macAddress);
  buffer.add(checksumCalculator(buffer.sublist(3), buffer.sublist(3).length));
  print("Buffer  qr'lı: " + buffer.toString());

  return buffer;
}

List<int> getbufferwithmacaddressDinamik(List<int> buffer, List<int> macAddress) {
  print("Buffer  qr'lı: " + buffer.toString());

  buffer.replaceRange(20, 26, macAddress);
  buffer.removeLast();
  buffer.add(checksumCalculator(buffer.sublist(3), buffer.sublist(3).length));
  // buffer[buffer.length - 1] = checksumCalculator(buffer.sublist(3), buffer.sublist(3).length);
  print("Buffer  qr'lı: " + buffer.toString());

  return buffer;
}

Future<List<int>> readQR() async {
  List<int> macAddressQR = [];
  List<String> macAdressQRString = [];
  String barcodeScanRes;
  try {
    barcodeScanRes = await barcode.FlutterBarcodeScanner.scanBarcode('#ff6666', 'İptal', true, barcode.ScanMode.QR);
    print(barcodeScanRes);
  } on PlatformException {
    barcodeScanRes = 'Failed to get platform version.';
  }

  print("Okunan Mac Adresi: $barcodeScanRes");

  for (var i = 0; i < barcodeScanRes.length / 2; i++) {
    macAdressQRString.add("0x" + barcodeScanRes.substring(i * 2, i * 2 + 2));
  }
  for (var i = 0; i < macAdressQRString.length; i++) {
    int a = int.parse(macAdressQRString[i]);

    macAddressQR.add(a);
  }

  print("Gönderilen Mac Adresi: $macAddressQR");
  return macAddressQR;
}

String getDeviceTypeString(int value) {
  if (value == 1) {
    return "1: Trailer";
  } else if (value == 2) {
    return "2: Harici Sıcaklık Sensörlü";
  } else if (value == 3) {
    return "3: Dahili Sıcaklık ve Nem Sensörlü";
  } else
    return "null";
}

class DinamikListModel {
  final int? id;
  final String title;
  final String byteValue;
  final bool isClient;
  DinamikListModel({
    this.id,
    required this.title,
    required this.byteValue,
    required this.isClient,
  });

  @override
  String toString() {
    return 'DinamikListModel(id: $id, title: $title, byteValue: $byteValue, isClient: $isClient)';
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'byteValue': byteValue,
      'isClient': isClient == true ? 1 : 0,
    };
  }

  factory DinamikListModel.fromMap(Map<String, dynamic> map) {
    return DinamikListModel(
      id: map['id'],
      title: map['title'],
      byteValue: map['byteValue'],
      isClient: map['isClient'] == 0 ? false : true,
    );
  }

  String toJson() => json.encode(toMap());

  factory DinamikListModel.fromJson(String source) => DinamikListModel.fromMap(json.decode(source));
}
