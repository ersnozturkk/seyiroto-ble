class BeaconData {
  double temp1;
  double temp2;
  DateTime date;
  int doorStatus;

  BeaconData({
    required this.temp1,
    required this.temp2,
    required this.date,
    required this.doorStatus,
  });

  @override
  String toString() {
    return 'BeaconData(temp1: $temp1, temp2: $temp2, date: $date, doorStatus: $doorStatus)';
  }
}
