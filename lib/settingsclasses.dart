class Settings {
  Settings({
    this.currenttime,
    this.externalwawtchdogresetcount,
    this.internalwawtchdogresetcount,
    this.softresetcount,
    this.cPULockUPresetcount,
    this.connectionedtimewithAP,
    this.downloadedfilecount,
    this.totaldownloadingfiletime,
    this.totaladvertisingcount,
    this.deviceType,
    this.deviceConnectiontype,
    this.logPeriod,
    this.txPower,
    this.firstrunningtime,
    this.battaryvoltage,
    this.applicationVersion,
    this.softDeviceversion,
  });

  DateTime? currenttime;
  int? externalwawtchdogresetcount;
  int? internalwawtchdogresetcount;
  int? softresetcount;
  int? cPULockUPresetcount;
  Duration? connectionedtimewithAP;
  int? downloadedfilecount;
  Duration? totaldownloadingfiletime;
  int? totaladvertisingcount;
  //2.ayarlar
  Duration? advertisingPeriod;
  int? deviceType;
  String? deviceConnectiontype;
  Duration? logPeriod;
  int? txPower;
  //3.ayarlar
  DateTime? firstrunningtime;
  double? battaryvoltage;
  double? applicationVersion;
  String? softDeviceversion;
}

Settings settings = new Settings();
