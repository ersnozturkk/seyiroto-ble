import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String buttonText;
  final VoidCallback onPress;
  final Color buttonColor;

  final Color clickedButtonColor;
  final double buttonRadius;
  final double height;
  final Widget? buttonIcon;
  final TextStyle? textStyle;
  final Key? buttonKey;

  CustomButton({
    required this.buttonText,
    required this.onPress,
    this.buttonColor = Colors.blue,
    this.clickedButtonColor = Colors.grey,
    this.buttonRadius = 15,
    this.height = 50,
    this.buttonIcon,
    this.textStyle,
    this.buttonKey,
  });

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(widget.buttonRadius),
      child: ElevatedButton(
        key: widget.buttonKey,
        onPressed: () async {
          widget.onPress();
        },
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(widget.buttonColor),
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            minimumSize: MaterialStateProperty.all(
                Size(double.infinity, widget.height))),
        child: Container(
          height: widget.height,
          alignment: Alignment.center,
          decoration: BoxDecoration(),
          child: widget.buttonIcon == null
              ? Text(
                  widget.buttonText.toString(),
                  style: widget.textStyle,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      child: widget.buttonIcon,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      widget.buttonText.toString(),
                      style: widget.textStyle,
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
